# Multiphase 3D volcanic explosion modeling

**PD leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/INGV.png?inline=false" width="35">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/Cineca.png?inline=false" width="80">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/csic.png?inline=false" width="80">

**Codes:** 

**Derived Simulation Cases:** [SC6.1](https://gitlab.com/cheese5311126/simulation-cases/sc6.1), [SC6.2](https://gitlab.com/cheese5311126/simulation-cases/sc6.2)

## Description

Recent events at Mt. Ontake (Japan, 2014) and Whakaari/White Island (New Zealand, 2019) showed
that the hazard associated with small-scale, potentially frequent, phreatic explosions has been underestimated, partly due to
the general lack of long/medium term precursory geophysical signals and of numerical models specifically designed to
simulate these processes. These aspects require high-resolution simulations of explosions associated with water flashing
(phreatic explosions) typical of magmatic and hydrothermal systems in volcanic islands and calderas, the quantification of
their hazards (particularly, pyroclastic flows and ballistic ejection) and the development of UC procedures to produce maps
of hazardous actions from phreatic eruptions during volcanic unrests.

## Objective

The main objectives of this PD are: (1) develop a multiscale simulation capability (from sub-meter scale for the
initial water flashing phase to kilometer scale for the flow and ballistic impacted area) of explosive processes, improving
unstructured mesh discretization methods (potentially including adaptive meshes) and sub-grid scale modeling, (2) develop
physical models for mechanical and thermal non-equilibrium multiphase eruptive mixtures and for the temperature evolution
of ballistic blocks and, (3) exploring two- and four-way coupling of coarse Lagrangian ballistic particles with Eulerian fine
ash and gas phases in the eruptive mixture. This will result in the development of an UC workflow (pre- syn- and
post-processing and visualization) for deterministic scenario definition and probabilistic hazard mapping workflow for
phreatic eruptions on volcanic islands and calderas, including uncertainty quantification.
